document.addEventListener("DOMContentLoaded", (event) => {
    target = document.querySelector('#widgetils');
    target.innerHTML = '';

	var list = {
		"icone": [
			{
				"sito": "//www.ils.org/",
				"icona": "ils.png",
				"alt": "Italian Linux Society",
				"sub": "Dal 1994, Linux in Italia"
			},
			{
				"sito": "//www.linux.it",
				"icona": "linux.png",
				"alt": "Linux.it",
				"sub": "Inizia da qui per scoprire Linux"
			},
			{
				"sito": "//lugmap.linux.it/",
				"icona": "lugmap.png",
				"alt": "LugMap",
				"sub": "Mappa degli User Group"
			},
			{
				"sito": "//www.linuxday.it/",
				"icona": "linuxday.png",
				"alt": "Linux Day",
				"sub": "A fine ottobre, in tutta Italia"
			},
			{
				"sito": "//forum.linux.it/",
				"icona": "forum.png",
				"alt": "Forum",
				"sub": "Assistenza e opinioni"
			},
			{
				"sito": "//scuola.linux.it/",
				"icona": "scuola.png",
				"alt": "Scuola",
				"sub": "Liberi di imparare"
			},
			{
				"sito": "//planet.linux.it/",
				"icona": "planet.png",
				"alt": "Planet Linux",
				"sub": "News e aggiornamenti"
			},
			{
				"sito": "//www.linuxsi.com/",
				"icona": "linuxsi.png",
				"alt": "LinuxSi",
				"sub": "I negozi Linux-friendly"
			}
		]
	};

	list['icone'].forEach((sito) => {
		target.insertAdjacentHTML( 'beforeend', '<div style="position: relative; margin-bottom: 5px">\
		<a href="' + sito['sito'] + '">\
		<img src="//www.ils.org/external/icons/' + sito['icona'] + '" width="40" style="margin-right: 10px" loading="lazy" alt="' + sito['alt'] + '" />\
		<span style="position: absolute; top: 12px">' + sito['alt'] + '</span>\
		<span style="position: absolute; top: 25px">' + sito['sub'] + '</span>\
		</a>\
		</div>');
	});
});
