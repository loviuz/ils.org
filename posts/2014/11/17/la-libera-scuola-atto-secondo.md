<!--
.. title: La Libera Scuola: Atto Secondo
.. slug: la-libera-scuola-atto-secondo
.. date: 2014-11-17 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy: 
.. previewimage:
-->

Si è conclusa sabato la consultazione pubblica <a rel="nofollow" href="http://labuonascuola.gov.it/">"La Buona Scuola"</a>, indetta dal Ministero dell'Istruzione per raccogliere spunti e proposte per migliorare e rinnovare il sistema scolastico italiano. Cui <a href="/">Italian Linux Society</a> (col <a href="{% link _posts/2014-10-29-la-libera-scuola-aggiornamento.md %}">sostegno</a> di <a rel="nofollow" href="http://wikimedia.it/">Wikimedia Italia</a>) ha risposto con "La Libera Scuola": cinque proposte semplici ed immediatamente attuabili per favorire e supportare la diffusione del software e dei contenuti liberi in un contesto sensibile come la scuola.

Le proposte hanno ricevuto un gran numero di voti e commenti, primeggiando nelle rispettive categorie, e più in generale non sono mancati riferimenti al software libero e aperto anche in altri propositi pubblicati sul portale. E' evidente un diffuso interesse ed una generale attenzione sul tema, che devono però ora trovare un riscontro pratico e tangibile.

Pertanto abbiamo oggi spedito due mail agli indirizzi di riferimento per le categorie <a rel="nofollow" href="https://labuonascuola.gov.it/costruiamo-insieme/meno-costi-per-le-famiglie/">"Meno costi per le famiglie"</a> e <a rel="nofollow" href="https://labuonascuola.gov.it/costruiamo-insieme/programma-digital-makers/">"Programma Digital Makers"</a> di "La Buona Scuola", in cui si trovano le due proposte che hanno destato maggiori reazioni (rispettivamente <a rel="nofollow" href="https://labuonascuola.gov.it/area/m/1778/">quella sulla validazione dei libri distribuiti con licenza libera</a> e <a rel="nofollow" href="https://labuonascuola.gov.it/area/m/1779/">quella sul catalogo di applicazioni libere certificate dal Ministero</a>), per richiedere e sollecitare un intervento operativo su questi due fronti.

Di seguito i testi delle due lettere inviate, cui speriamo di ricevere presto risposta (e sulle quali continamo di tornare ad aggiornarvi presto).

<blockquote>
Buongiorno,
scrivo per conto di Italian Linux Society, l'associazione promotrice de "La Libera Scuola", campagna parallela a "La Buona Scuola" con la quale abbiamo avanzato e sostenuto cinque proposte in favore del software e dei contenuti liberi presso la popolare consultazione che si è chiusa sabato.

Siamo lieti di constatare come alcune di queste abbiano avuto un discreto riscontro, ed in particolare quella dedicata alla pubblicazione di un catalogo di applicativi liberi e opensource specifici per la didattica "promossi" direttamente dal Ministero per l'Istruzione.
https://labuonascuola.gov.it/area/m/1779/
Lo scopo di questa iniziativa sarebbe fornire un semplice ed immediato elenco di possibilità, selezionate tra quelle più utili e diffuse, che invoglino i docenti nostrani ad adottare o quantomeno provare applicazioni software libere (e dunque analizzabili a fini didattici, legalmente ridistribuibili agli studenti per l'utilizzo domestico, e sicure dal punto di vista della tutela delle informazioni) per lo svolgimento delle loro attività.

Certo esistono già diversi "cataloghi" reperibili online e pubblicati da enti più o meno istituzionali, ma un riconoscimento diretto ed esplicito del Ministero nei confronti di un insieme mirato di applicativi sarebbe di grande aiuto per la popolosa comunità italiana a supporto al software libero, che ogni giorno opera con accrescere la consapevolezza sulla tecnologia e non di rado già oggi aiuta materialmente molte scuole per l'allestimento e la manutenzione di laboratori informatici didattici, in quanto rappresenterebbe un utilissimo strumento per favorire ed accelerare tale vitale processo.

Siamo naturalmente a disposizione per collaborare su tale progetto, magari anche coinvolgendo la comunità WiiLDOS (la più grande ed attiva sul tema del software libero nella scuola, composta da centinaia di insegnanti e tecnici sensibili all'argomento) per stendere una prima bozza da portare alla vostra attenzione e valutare insieme.

Grazie per l'attenzione, la collaborazione e l'opportunità di poter contribuire a migliorare il nostro sistema scolastico.
</blockquote>

<blockquote>
Buongiorno,
scrivo per conto di Italian Linux Society, l'associazione promotrice de "La Libera Scuola", campagna parallela a "La Buona Scuola" con la quale abbiamo avanzato e sostenuto cinque proposte in favore del software e dei contenuti liberi presso la popolare consultazione che si è chiusa sabato.

Siamo lieti di constatare come alcune di queste abbiano avuto un discreto riscontro, ed in particolare quella dedicata alla promozione dei libri di testo distribuiti in licenza libera.
https://labuonascuola.gov.it/area/m/1778/
L'intento di questa proposta sarebbe quello di far emergere le numerose possibilità offerte dal patrimonio comune liberamente reperibile sul web, collaborativamente redatto e revisionato, già predisposto per l'utilizzo scolastico, da ciascuno acquisibile, modificabile nel formato e nei contenuti, fruibile in formato digitale o stampabile in proprio a costi contenuti.

Purtroppo spesso tali risorse sono ignorate o peggio ancora ostacolate - per evidenti conflitti di interesse col mondo dell'editoria classica... - pur rappresentando una grande opportunità sia culturale che economica, ma certo la pubblicazione di una oggettiva e scientifica revisione da parte del Ministero per l'Istruzione di alcuni di questi libri permetterebbe una seria valutazione di tali opere e ne faciliterebbe la loro diffusione, con beneficio di tutti (in primis delle famiglie).

Saremmo contenti di potervi inviare un paio di copie di Matematica C3 Algebra 1 Quinta Edizione, probabilmente uno dei più noti e popolari libri di testo in licenza libera ad oggi disponibili: recentemente ne abbiamo stampate alcune copie, appunto da far circolare a scopo dimostrativo, ogni volume è costato circa 9 euro IVA e spedizione incluse (ma chiunque può rivolgersi presso la stamperia che preferisce, il materiale è scaricabile gratuitamente e senza limitazioni da Internet) e appunto ciascun insegnante può liberamente scegliere quali parti stampare o magari fascicolarlo per renderlo più maneggevole.

Grazie per l'attenzione, la collaborazione e l'opportunità di poter contribuire a migliorare il nostro sistema scolastico.
</blockquote>

Grazie a tutti coloro che hanno sostenuto le proposte de "La Libera Scuola", contribuendo a dar loro forza e diffusione!