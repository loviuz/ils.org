<!--
.. title: Crescita Digitale 2014-2020
.. slug: crescita-digitale-2014-2020
.. date: 2014-12-18 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy: 
.. previewimage:
-->

A metà novembre l'<a rel="nofollow" href="http://www.agid.gov.it/">Agenzia per l'Italia Digitale</a>, ente strumentale della Presidenza del Consiglio rivolto alla promozione e all'accelerazione della digitalizzazione delle pubbliche amministrazioni e delle imprese italiane, ha pubblicato <a rel="nofollow" href="http://www.agid.gov.it/sites/default/files/documenti_indirizzo/crescita_digitale_2020.pdf">un documento programmatico</a> che riassume le strategie chiave che verranno adottate nel prossimo lustro per accrescere il ruolo delle tecnologie nel nostro Paese, ma anche la consapevolezza nei loro confronti ed il loro impatto sull'economia locale.

Incredibilmente tra i fattore determinanti enumerati nel documento non appare sotto nessuna forma il software libero ed opensource, che invece è stato riconosciuto in altre sedi - non ultime, le Linee Guida emanate dall'Agenzia stessa nel 2013 <a rel="nofollow" href="http://www.agid.gov.it/notizie/riuso-valutazione-comparativa-online-la-circolare">per il riuso di software</a> - come elemento di valenza strategica per garantire interoperabilità, indipendenza e sostenibilità degli strumenti tecnologici adottati dalle amministrazioni pubbliche.

Di seguito la risposta di <a href="/">Italian Linux Society</a> alla <a rel="nofollow" href="http://commenta.formez.it/ch/crescita_digitale/">consultazione</a> attualmente in corso sui contenuti del suddetto piano strutturale, con alcune considerazioni di ordine generale ed alcune proposte mirate.

<blockquote>
Il documento “Strategia per la Crescita Digitale 2014-2020” del 6 novembre 2014, condiviso per la pubblica consultazione sul sito dell'Agenzia per l'Italia Digitale, pur affrontando il tema della digitalizzazione del nostro Paese sotto numerosi ed importanti aspetti manca a nostro avviso di citare un fattore cui ampio spazio era invece stato dato da precedenti assetti strategici dello stesso tenore e che, soprattutto negli ultimi mesi, sempre più ha interessato Pubbliche Amministrazioni di ogni dimensione e grado, italiane ed europee: il software libero e opensource.

Dati i benefici (empirici e teorici, di breve e lungo periodo, tecnici e monetari) portati sia all'amministrazione che al sistema economico dall'adozione di tale modello di sviluppo e condivisione – certamente già noti ad AgID – sorprende constatare che tale princìpio non sia affatto contemplato in un documento le cui premesse ed i cui obiettivi sono i medesimi che da sempre vengono associati ad esso.
Come può crescere l'economia locale del Paese se i fondi da destinare al settore vengono spesi per acquistare prodotti esteri, o il cui costo di licenza è spesso reiterato nel tempo senza dare alcun valore aggiunto? Come possono crescere le competenze se Ricerca & Sviluppo sono sistematicamente delegate ad un pugno di società fondate sul monopolio, il lock-in e la segretezza? Come è possibile intervenire in modo rapido ed efficiente sull'interoperabilità delle procedure e delle informazioni tra diverse amministrazioni se algoritmi e formati non sono immediatamente accessibili e condivisi da parte di chi vuole implementarli ed integrarli in nuove ed innovative soluzioni?

Per questi e per altri motivi, chiediamo che all'interno del documento programmatico in oggetto:
<ul>
<li>vengano esplicitati l'impiego di licenze libere e l'adesione al modello di sviluppo opensource come requisiti per il software sviluppato ed adottato nel contesto del piano strategico sotto la supervisione e con il contributo di AgID</li>
<li>la base applicativa dell'Anagrafe della Popolazione Residente, ed i relativi protocolli e componenti, siano pubblicati con una licenza libera e aperta, per permettere l'integrazione con altri applicativi paralleli e complementari vitali per il funzionamento delle amministrazioni locali</li>
<li>le risorse destinate alla formazione sulle competenze digitali siano espressamente in favore non di specifici prodotti di specifici vendor ma di soluzioni libere, aperte, pubbliche e riusabili</li>
<li>venga inclusa tra i parametri di monitoraggio la ricaduta sul territorio delle spese fatte in campo ICT da parte delle amministrazioni: i soldi spesi in licenze di applicazioni prodotte e vendute da soggetti esteri rappresentano una perdita secca nella bilancia economica, e di nessun vantaggio per la comunità locale</li>
</ul>

In questo quadro di riferimento, e data la necessità evidente di una radicale accelerazione in tema di digitalizzazione dell'amministrazione, invitiamo l'Agenzia per l'Italia Digitale a valutare anche la possibilità (e l'opportunità) di dedicare parte della propria dotazione finanziaria all'implementazione di uno stack applicativo completo orientato ai piccoli e medi comuni, da sviluppare secondo i canoni del modello opensource e da rilasciare in licenza libera, da mettere a disposizione degli enti stessi e degli operatori di mercato a loro supporto. Uno strumento di tal fatta rappresenterebbe un veloce, efficiente e vantaggioso canale per esporre ai soggetti più modesti – e dunque individualmente con minori risorse economiche, tecniche ed umane – tutte le funzioni auspicate oggi nella Strategia, già integrate con i protocolli e le interfacce previste e promosse da AgID, da far successivamente evolvere in maniera controllata eppur coordinata; esso sarebbe il “sistema operativo” della PA in Italia.

La Crescita Digitale deve necessariamente passare per una evoluzione di carattere culturale, per cui la soluzione tecnica e tecnologica non sia vista come un prodotto ma come una risorsa. Da condividere, integrare, espandere e migliorare. Non un traguardo fine a sé stesso, ma una base su cui costruire.
</blockquote>