<!--
.. title: Arriva la nuova squadra
.. slug: nuovo-direttivo-2024
.. date: 2024-05-16 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy:
.. previewimage: /images/posts/direttivo_ils_2024.jpg
-->

Un grandissimo grazie al presidente uscente Roberto Guido. Nuova squadra, nuove sfide.

<!-- TEASER_END -->

Sabato 5 aprile 2024 si è tenuta l'assemblea per la nomina del direttivo. Il presidente Roberto Guido annuncia di non volersi ricandidare, dopo 12 anni di onorato servizio, concludendo il suo ultimo mandato con il raggiungimento del massimo storico di 248 soci.

## La nuova squadra

Il nuovo direttivo di Italian Linux Society che durerà 3 anni, da aprile 2024 ad aprile 2027, è così composto:

* Valerio Bozzolan (presidente) · nome utente ILS: `gnu`
* Fabio Lovato (direttore) · `loviuz`
* Daniele Scasciafratte (consigliere) · `mte90`
* Luisa Ravelli (consigliere) · `naima`
* Federica Di Lauro (consigliere) · `fdila`

Questo nuovo consiglio direttivo sostituisce quello precedente (da aprile 2021 ad aprile 2024) che era composto da: Roberto Guido (presidente), Valerio Bozzolan (direttore), Daniele Scasciafratte, Alessandro Rubini, Matteo Ruffoni.

La nuova squadra si è impegnata a continuare a garantire stabilità all'associazione, offrendo rinnovata energia per accrescere la consapevolezza sui benefici pratici, etici, economici e sociali del software libero, e portarlo a nuovi livelli in Italia.

In ogni caso, Italian Linux Society non è il suo direttivo. Italian Linux Society è in tutte le persone associate, tutte le sezioni locali, le organizzazioni che accomunano parte dei nostri scopi e il tuo aiuto (grazie, che stai leggendo!).

Per maggiori informazioni sul ruolo del consiglio direttivo, vedi lo [statuto](/statuto/).

## Primi 40 giorni

Fra le varie sfide dei primi 40 giorni di lavoro di questo nuovo direttivo, c'è un interessante quantità di passaggi di consegne, su una bella fetta della nostra infrastruttura.

Per chi desidera una visione d'insieme della nostra infrastruttura, eccola:

<https://gitlab.com/ItalianLinuxSociety/ils-infrastructure/-/blob/main/INVENTORY.md>

Nel tentativo di aumentare la conoscenza condivisa su come funzioniamo, è stato aperto un certo numero di repository e tag su GitLab. Decisamente una bella quantità di cose a fare per chiunque:

<https://gitlab.com/groups/ItalianLinuxSociety/-/boards>

Per esempio, è in corso il ripristino del [servizio di consegna adesivi di Italian Linux Society](https://www.linux.it/stickers/) che era da sempre gestito dall'eroico Roberto Guido nel suo tempo libero.

Sarà ora il volontario Ferdinando Traversa a prendere le redini dell'ufficio postale di ILS. Verrà ripristinato probabilmente la settimana del 21 maggio 2024, ed entrerà a pieno regime nelle settimane successive, spedendo circa 60 nuove lettere pendenti a tutta Italia:

<https://gitlab.com/ItalianLinuxSociety/direttivo/-/issues/1>

Linux Day, Sabato 26 ottobre 2024: si sta cercando di anticipare un preventivo per i gadget nazionali, con l'obiettivo di aumentarli di molto, rispetto all'anno scorso. Ne riparleremo:

<https://gitlab.com/ItalianLinuxSociety/direttivo/-/issues/27>

Chiediamo inoltre a ogni persona associata di dare pieno supporto psicologico al [JET LUG](https://www.jetlug.it/) di Arona, che da questi mesi non riesce più ad ottenere gratuitamente le sedi dal suo comune e ha bisogno di supporto da ILS:

<https://gitlab.com/ItalianLinuxSociety/direttivo/-/issues/26>

E altre cose interessanti, sempre sulla [board di ILS su GitLab](https://gitlab.com/groups/ItalianLinuxSociety/-/boards).

## Dove stiamo andando

Lo scopo dell'associazione è, da sempre, quello di **promuovere e divulgare Linux e il software libero e open source**. Negli ultimi 10 anni, il panorama digitale è cambiato tantissimo nel mondo in cui viviamo, ed è per questo che c'è bisogno sempre più di mantenere alta l'attenzione su ciò che accade intorno a noi, per riuscire a indirizzare gli sforzi che, ogni giorno, volontari e volontarie svolgono sul territorio.

**LUG** e **sezioni locali ILS** si impegnano costantemente per portare Linux e la filosofia del software libero dove possibile: nelle scuole, nelle famiglie, fra le persone in difficoltà, e spingono così anche al riuso di vecchio hardware o dispositivi in disuso, riducendo i rifiuti elettronici, in molti casi "solo" perché il nuovo software (non libero) decide di non supportare più quel "vecchio rottame".

Questa comunità ha al suo interno persone che **sviluppano software**, **traducono applicazioni**, creano **videogiochi** e pubblicano **contenuti liberi** dal diritto d'autore, senza contare attivisti nel campo **privacy**, oppure **makers** che, con la stessa filosofia hacker e fai-da-te (DIY), promuovono anche tecnologia, arte, ingegneria e molto altro, con lo stesso spirito libero e open source.

Queste persone, che spendono il loro tempo libero in tutte queste fantastiche attività, permettono di realizzare i sogni che abbiamo: vivere in un mondo digitale libero, aperto e protetto. Proprio per questo, come Italian Linux Society, il nostro impegno sarà quello di continuare a supportare queste realtà e di creare insieme progetti di divulgazione e promozione.

## Sezioni locali

Dal 2020 sono nate le sezioni locali ILS, un modo per creare, nel proprio territorio, un gruppo attivo che non necessita di aprire un'associazione vera e propria, così da lasciare che le persone siano concentrate nell'organizzazione di incontri, eventi e attività più "operative".
La sezione locale ILS permette, infatti, di creare un gruppo locale in tutta Italia che può operare senza la burocrazia di un'associazione registrata, ma con una serie di vantaggi, come la richiesta di patrocinio, acquisti e [rimborsi per le attività](/info/rimborsi/) e altro che si può trovare nella pagina riguardante il [funzionamento delle sezioni locali](/sezionilocali/).

## Dove trovare Italian Linux Society

I principali canali dove trovare tutti i soci e simpatizzanti sono:

* [ILS sul Fediverso](https://mastodon.uno/@ItaLinuxSociety): per seguire Italian Linux Society su Mastodon (#SoftwareLibero)
* [ILS su Telegram](https://t.me/ItalianLinuxSociety): chat operativa
* [forum.linux.it](https://forum.linux.it/): forum ufficiale per conoscersi, chiedere assistenza, e annunciare progetti o attività
* [ILS - mailing list soci](https://lists.linux.it/listinfo/soci): iscriviti usando la tua casella `@linux.it`
* [ILS su Gitlab](https://gitlab.com/ItalianLinuxSociety): repository ufficiale con tutti i progetti e le loro *issue*

Ricordiamo che Italian Linux Society non ha alcun membro dello staff pagato. [Dona Italian Linux Society](/info/#donazioni) per investire nei progetti nella community e aiuta le persone ad associarsi nella loro sezione locale più vicina, per dare loro maggiore sostegno:

<https://ilsmanager.linux.it/ng/register>

## Crediti fotografici

Foto con licenza CC BY 2.0 di [pestoverde](https://openverse.org/image/collection?source=flickr&creator=pestoverde) da [Openverse](https://openverse.org/image/b0d2d66e-4452-4a8f-b57b-598aa3240718), modificata usando il software libero di fotoritocco [GIMP](https://it.wikipedia.org/wiki/GIMP).
